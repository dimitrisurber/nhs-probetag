class PDFReplaceStageLinksToLive {
  constructor(config = {}) {
    const {
      anchors
    } = config;

    this.anchors = Array.prototype.slice.call(document.querySelectorAll(anchors));

    if(this.anchors && this.anchors.length > 0){
      this.bindEvents();
    }
  }

  bindEvents() {
    this.anchors.forEach((element) => {
    const reg = /(https:\/\/report-[Clientname]-stage.nswow.ch)/g; //replace [Clientname]. The URL must be the same as you have on stage
    let href = element.href;
    if(href.match(reg)){
        let hr = href.replace(reg, 'https://report.[Clientname].[net/com/ch]'); //replace [Clientname].
        element.href = hr;
        if(element.innerHTML.match(reg)){
            let txt = element.innerHTML.replace(reg, 'https://report.[Clientname].[net/com/ch]');
            element.innerHTML = txt;
        }
      }
    });
  }
}

export default PDFReplaceStageLinksToLive;
