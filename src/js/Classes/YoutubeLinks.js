import Helper from './Helper';

class YoutubeLinks {

  constructor(config = {}) {
    const {
      videoContainerSelector
    } = config;

    this.videoContainer = document.querySelectorAll(videoContainerSelector);
    this.videoContainerArray = Array.prototype.slice.call(this.videoContainer);
    if (this.videoContainer) {
      this.videoContainerArray.forEach(container => {
        this.addLink(container);
      });
    }
  }

  addLink(videoContainer) {
    const videoIframe = videoContainer.querySelector('.ns-video__video iframe');
    const videoSource = videoIframe.src;

    const aTag = document.createElement('a');
    aTag.classList.add('ns-video__link');
    aTag.setAttribute('href', videoSource);
    Helper.wrap(videoContainer, aTag);
  }

}

export default YoutubeLinks;
