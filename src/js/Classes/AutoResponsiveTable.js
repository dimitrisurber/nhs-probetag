import Helper from './Helper';

class AutoResponsiveTable {
  constructor(config = {}) {
    const {
      tableWrapperSelector,
      noResponsiveClass,
    } = config;
    this.noResponsiveClass = noResponsiveClass || 'js-no-responsive-table';
    this.tableWrapperSelector = tableWrapperSelector || '.ns-table-wrap';

    this.tables = document.querySelectorAll(`${tableWrapperSelector}:not(.${noResponsiveClass})`);
    this.tablesArray = [...this.tables];
    this.styleTagArry = [];
    this.colgroupTagArray = [];

    if (this.tables) {
      this.getNaturalWidths();
      this.bindEvents();
      if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
        var evt = document.createEvent('UIEvents');
        evt.initUIEvent('resize', true, false, window, 0);
        window.dispatchEvent(evt);
      } else {
        window.dispatchEvent(new Event('resize'));
      }
    }
  }

  bindEvents() {
    window.addEventListener('resize', () => {
      const vw = window.innerWidth;
      this.tablesArray.forEach((tableWrapper, index) => {
        const breakPoint = tableWrapper.getAttribute('data-max-width');
        const breakPointFormat = tableWrapper.getAttribute('data-format-max-width');
        const hasClone = tableWrapper.querySelector('.clone');

        if (vw < breakPointFormat) {
          this.removeFormats(index);
        } else {
          this.addFormats(index);
        }

        if (vw < breakPoint) {
          if(!hasClone){
            this.createResponsiveMarkup(tableWrapper);
          }
        } else {
          if(hasClone){
            this.restoreMarkup(tableWrapper);
          }
        }
      });
    });
  }

  getNaturalWidths() {
    this.tablesArray.forEach((tableWrapper, index) => {
      const table = tableWrapper.querySelector('table');
      const style = table.querySelector('style');
      const colgroup = table.querySelector('colgroup');
      if (style) {
        this.styleTagArry.push({tableIndex: index, styleTag: style});
        this.colgroupTagArray.push({tableIndex: index, colgroupTag: colgroup});
        style.remove();
        colgroup.remove();
      }
      tableWrapper.setAttribute('style', 'width: 1920px;'); // set this to large width to properly calculate natural max width of table
      table.setAttribute('style', 'table-layout:auto;width: auto;');
      const naturalWidth = table.offsetWidth;
      tableWrapper.setAttribute('data-max-width', naturalWidth);
      table.prepend(style);
      table.prepend(colgroup);
      const formatMaxWidth = table.offsetWidth;
      tableWrapper.setAttribute('data-format-max-width', formatMaxWidth);
      tableWrapper.removeAttribute('style');
      table.removeAttribute('style');
    });
    console.log(this.colgroupTagArray);
  }

  createResponsiveMarkup(tableWrapper) {
    const table = tableWrapper.querySelector('table');
    if (table.nextSibling !== null) {
      this.moveFootnotes(table.nextSibling);
    }
    const cellsArray = [...table.querySelectorAll('tr td:first-of-type, tr th:first-of-type')];
    cellsArray.forEach(cell => {
      Helper.addClass(cell, 'fixed');

      if (cell.rowSpan >= 2) {
        const numRowSpan = parseInt(cell.rowSpan);
        const currentRow = this.getChildIndex(cell.parentElement);

        for (let i = 0; i <= numRowSpan; i++) {
          const selector = table.querySelector(`tr:nth-of-type(${currentRow+i}) td:first-of-type:not([rowspan="${numRowSpan}"])`);
          if (selector) {
            Helper.addClass(selector, 'unfixed');
          }
        }
      }

      if (Helper.hasClass(cell, 'fixed') && Helper.hasClass(cell, 'unfixed')) {
        Helper.removeClass(cell, 'fixed');
      }
    });

    Helper.addClass(tableWrapper, 'js-responsive-table');

    const clone = table.cloneNode(true);
    Helper.addClass(clone, 'clone');
    const parent = table.parentNode;

    parent.parentNode.insertBefore(clone, parent.nextSibling);
  }

  moveFootnotes(footnote) {
    footnote.parentNode.parentNode.appendChild(footnote);
  }

  restoreMarkup(tableWrapper) {
    Helper.removeClass(tableWrapper, 'js-responsive-table');
    if(tableWrapper.querySelector('.clone')){
      tableWrapper.querySelector('.clone').remove();
    }
  }

  removeFormats(tableIndex) {
    const table = this.tablesArray[tableIndex].querySelector('table');
    if(table.querySelector('style')){
      table.setAttribute('style', 'table-layout:auto;');
      table.querySelector('style').remove();
      table.querySelector('colgroup').remove();
    }
  }

  addFormats(tableIndex) {
    const table = this.tablesArray[tableIndex].querySelector('table');
    if(!table.querySelector('style')){
      table.removeAttribute('style');
      this.styleTagArry.forEach(tag => {
        if(tag.tableIndex == tableIndex){
          table.prepend(tag.styleTag);
        }
      });

      console.log(this.colgroupTagArray);
      this.colgroupTagArray.forEach(tag => {
        if(tag.tableIndex == tableIndex){
          table.prepend(tag.colgroupTag);
        }
      });
    }
  }
}

export default AutoResponsiveTable;
