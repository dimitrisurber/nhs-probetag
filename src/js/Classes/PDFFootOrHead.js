import Helper from './Helper';
class PDFFootOrHead {
  constructor(config = {}) {
    const {
      prepend,
      append,
      elementToAddTo,
      cssClass,
      elementTagName,
      elementInnerHTML
    } = config;

    this.prepend = prepend || true;
    this.append = append || false;
    this.elementToAddTo = elementToAddTo || document.querySelector('body');
    this.cssClass = cssClass;
    this.elementTagName = elementTagName || 'p';
    this.elementInnerHTML = elementInnerHTML;

    this.buildHeaderOrFooter();
  }

  buildHeaderOrFooter(){
    const element = document.createElement(this.elementTagName);
    if(this.cssClass){
      Helper.addClass(element, this.cssClass);
    }
    element.innerHTML = this.elementInnerHTML;

    if(this.prepend){
      this.elementToAddTo.prepend(element);
    }

    if(this.append){
      this.elementToAddTo.append(element);
    }
  }

} export default PDFFootOrHead;
