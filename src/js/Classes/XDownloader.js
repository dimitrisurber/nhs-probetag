import * as Excel from "exceljs/dist/exceljs.min.js";

import {
  saveAs
} from 'file-saver'

/**
 * Exceldownload working with NSWOW2 Tables
 */
class XDownloader {
  constructor(config = {}) {
    const {
      downloadText, // Text for downloadbutton, can be empty if you want to use an icon
      backgroundColor, // Color
      borderColor,
      borderStyle, // thin/dotted/medium/double/thick
      headClass,
      boldClass,
      backgroundClass,
      borderClass
    } = config;

    
    this.downloadText = downloadText || 'Download Excel'; //Text on button, can be empty
    this.backgroundColor = backgroundColor || 'ecf0f1';
    this.borderColor = borderColor || '34495e';
    this.borderStyle = borderStyle || 'thin';
    this.headClass = headClass || 'head';
    this.boldClass = boldClass || 'bold';
    this.backgroundClass = backgroundClass || 'bold';
    this.borderClass = borderClass || 'border-bottom';
    

    const tables = Array.prototype.slice.call(document.querySelectorAll('.ns-table-wrap'));

    tables.forEach(table => {
      this.addLink(table);
    });
  }

  createWorkbook(table) {
    const name = table.querySelector('[data-tableid]').dataset.tableid //if you have an error on this line replace data-tableid with data-table-id & tableid to tableId
    const workbook = new Excel.Workbook();
    this.worksheet = workbook.addWorksheet(name);
    this.parseTable(table);
    this.parseFootnote(table);
    this.saveFile(name, workbook);
  }

  /**
   * This one parses the footnotes and puts it after the table content in the worksheet
   * @param {*} table 
   */
  parseFootnote(table) {
    const footnotes = Array.prototype.slice.call(table.querySelectorAll('p.ns-style-footnote'));
    this.worksheet.addRow([]);
    footnotes.forEach(fn => {
      this.worksheet.addRow([fn.innerText.replace('\n', ' ')]);
    });
  }

  getTitle(elem) {

    // Get the next sibling element
    var sibling = elem.previousElementSibling;
  
    // If the sibling matches our selector, use it
    // If not, jump to the next sibling and continue the loop
    while (sibling) {
      if (sibling.matches('[class*="ns-title-"]')) return sibling;
      sibling = sibling.previousElementSibling;
    }
  
  };

  /**
   * This one parses the table and puts it into the worksheet
   * @param {*} table 
   */
  parseTable(table) {


    const t = table.querySelector('table');
    const rows = Array.prototype.slice.call(t.querySelectorAll('tr'));
    let row;
    let content = [];

    let counterR = 0;
    let counterC = 0;

    const title = this.getTitle(table);

    let styles = [];

    rows.forEach(r => {
      counterR++; //counters to keep track where you are
      counterC = 0;
      content = [];

      const tds = Array.prototype.slice.call(r.querySelectorAll('td'));
      if (r.classList.contains(this.headClass)) {

        tds.forEach(t => { //first iteration for header column without content to set widths of columns
          counterC++;

          content.push({
            header: ' ',
            key: 'id',
            width: t.offsetWidth / 10
          });

        });
        this.worksheet.columns = content;

        counterC = 0;
        counterR++;
        content = [];

        row = this.worksheet.lastRow;
        row.height = 0.5;
        
        if(title){
          this.worksheet.addRow([title.innerText]);
          row = this.worksheet.lastRow;
          
          row.getCell('A').font = {
            size: 16,
            bold: true
          };
          
          this.worksheet.addRow([]);
        } 
      }


      tds.forEach(t => { //second iteration for actual content
        counterC++;

        styles[counterC] = this.getStyles(t); // get styles based on classes of the td

        let c = this.correct(t.innerText);

        if (isNaN(c)) {
          c = c;
        } else {

          if (c == '\u00A0' || c.trim() == '') { //apparently this is still considered not NaN
            c = c;
          } else {
            c = parseFloat(c);
          }
        }
        content.push(c);
      });
      
      this.worksheet.addRow(content);

      row = this.worksheet.lastRow;

      //go through the styles array and apply the styles
      for (let i = 1; i < (styles.length + 1); i++) {
        if (!!styles[i]) {
          if (!!styles[i]['border']) {
            row.getCell(i).border = styles[i]['border'];
          }
          if (!!styles[i]['font']) {
            row.getCell(i).font = styles[i]['font'];
          }
          if (!!styles[i]['fill']) {
            row.getCell(i).fill = styles[i]['fill'];
          }
        }
      }
    });

  }

  /**
   * check for classes and return the right styles
   * can be extended
   * @param {*} t 
   */
  getStyles(t) {
    const r = [];

    if (t.classList.contains(this.backgroundClass)) {
      r['fill'] = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'FF'+this.backgroundColor
        }
      };
    } else { //if there's no background we return white background because it makes the excel prettier
      r['fill'] = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          rgb: 'FFFFFFFF'
        }
      };
    }

    if (t.classList.contains(this.boldClass) || t.classList.contains(this.headClass)) {
      r['font'] = {
        bold: true
      };
    } else {
      r['font'] = {};
    }

    if (t.classList.contains(this.borderClass)) {
      r['border'] = {
        bottom: {
          style: this.borderStyle,
          color: {
            argb: 'FF'+this.borderColor
          }
        }
      };
    } else {
      r['border'] = {};
    }

    return r;
  }


 /**
  * yeah well, add the fucking link to the table, this one doesnt need explanation
  * @param {*} table 
  */
  addLink(table) {
    let link = document.createElement('a');
    link.classList.add('exceldl-link');
    link.innerText = this.downloadText;
    link.href = '#';

    table.prepend(link);

    link.addEventListener('click',
      (e) => {
        e.preventDefault();
        this.createWorkbook(table);
      });
  }

  /**
   * savefile doesnt need explanation as well
   * @param {*} fileName 
   * @param {*} workbook 
   */
  async saveFile(fileName, workbook) {
    const xls64 = await workbook.xlsx.writeBuffer({
      base64: true
    })
    saveAs(
      new Blob([xls64], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      }),
      fileName
    )
  }

  /**
   * delete the thousand marks and replace false minus with right one
   */
  correct(text) {
    return text.replace(/'/g, '').replace(/\s/g, '').replace(/–/g, '-').replace(/,/g, '.');
  }

}
export default XDownloader;
