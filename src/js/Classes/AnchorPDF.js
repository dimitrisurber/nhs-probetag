import Helper from './Helper';

class AnchorPDF {
  constructor(config = {}) {
    const {
      anchorSelector
    } = config;
    this.links = document.querySelectorAll(anchorSelector);
    this.linksArray = Array.prototype.slice.call(this.links);
    if(this.links.length > 0){
      this.change();
    }
  }

  change(){
    this.linksArray.forEach((link, i) => {
      const idName = link.innerText;
      console.log(link);
      console.log(link.nextElementSibling);
      link.nextElementSibling.id = idName;
      link.remove();

      // const idName = link.innerText;
      // const aTag = document.createElement('a');
      // aTag.id = idName;
      // Helper.addClass(aTag, 'ns-anchor');
      // Helper.addAfter(aTag, link);
      // link.remove();
    });
  }

}

export default AnchorPDF;
