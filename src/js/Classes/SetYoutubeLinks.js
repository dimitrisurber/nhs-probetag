import Helper from './Helper';

// if the video-component in pdfReactor doesn't work and the article is not exportable, please check first, if there is an anchor-component before (doesn't need to be directly before) - that can cause troubles. And when not, check if the Firewall is the problem.
class SetYoutubeLinks {
  constructor(config = {}) {
    const {
      videoContainerSelector
    } = config;
    this.videoContainer = document.querySelectorAll(videoContainerSelector);
    this.videoContainerArray = Array.prototype.slice.call(this.videoContainer);
    if (this.videoContainer) {
      this.videoContainerArray.forEach(container => {
        this.addLink(container);
      });
    }
  }
  addLink(videoContainer) {
    const videoThumbnail = videoContainer.querySelector('.ns-video__thumbnail');
    const videoCode = videoContainer.querySelector('.ns-video__video a');
    const videoSource = videoCode.href;
    const aTag = document.createElement('a');
    aTag.classList.add('ns-video__link');
    aTag.setAttribute('href', videoSource);
    Helper.wrap(videoThumbnail, aTag);
  }
}
export default SetYoutubeLinks;
