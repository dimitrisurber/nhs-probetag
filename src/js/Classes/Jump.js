class Jump {
  constructor(config = {}) {
    const {
      graphicSelector,
    } = config;

    this.graphicsSelector = document.querySelectorAll(graphicSelector);
    this.graphicsSelectorArray = Array.prototype.slice.call(this.graphicsSelector);

    if (this.graphicsSelector) {
      this.bindEvents();
    }
  }

  bindEvents() {
    window.addEventListener('scroll resize', () => {
      this.graphicsSelectorArray.forEach(graphicItem => {
        console.log(graphicItem);
        this.toggleClassOnGraphic(graphicItem, e);
      });
    });
  }

  toggleClassOnGraphic(item, e) {
    if (Helper.isInViewport(item)) {
      Helper.toggleClass(item, '.jump-animate');
      item.addEventListener('scroll', e => {
        Helper.addClass(item, 'scrolled');
      });

      setTimeout(() => {
        if (!item.classList.contains('.jump-animate') && !item.classList.contains('.scrolled')) {
          Helper.addClass(item, '.jump-animate');
        }
      }, 4000);
    }
  }
}

export default Jump;

//to delete - just the function in jQuery to spick
/* $(document).ready(function(){
    $(window).scroll();
  });
  $(window).on('resize scroll', function () {
    $('.ns-img--jump').each(function(){
      if($(this).isInViewport()){


        var image = $(this);

        $(image).on('scroll', function(){ $(this).addClass('scrolled'); });

        setTimeout(function(){

          if(!image.hasClass('jump-animate') && !image.hasClass('scrolled')){
            image.addClass('jump-animate');
            image.animate({
              scrollLeft: '+=200px',
            }, 300, 'swing', function () {
              image.animate({
                scrollLeft: '-=200px',
              }, 500, 'easeOutBounce', function () {
              })
            });
          }
        }, 4000);

      }
    });
  }); */
