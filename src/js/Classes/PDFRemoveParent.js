import Helper from './Helper';
class PDFRemoveParent {
  constructor(config = {}) {
    const {
      parent
    } = config;
    this.parentArray = parent;
    if(this.parentArray.length > 0) {
      this.parentArray.forEach((parentItem, i)=>{
        parentItem = Array.prototype.slice.call(document.querySelectorAll(parentItem));
        if (parentItem.length > 0) {
          parentItem.forEach(e => {
            this.removeParent(e);
          });
        }
      });
    }
  }
  removeParent(e) {
    Helper.unwrap(e);
  }
}
export default PDFRemoveParent;
