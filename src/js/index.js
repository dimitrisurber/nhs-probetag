/**
 *                                          __  __
 *    ____  ______      ______ _      __   / /_/ /_  ___  ____ ___  ___
 *   / __ \/ ___/ | /| / / __ \ | /| / /  / __/ __ \/ _ \/ __ `__ \/ _ \
 *  / / / (__  )| |/ |/ / /_/ / |/ |/ /  / /_/ / / /  __/ / / / / /  __/
 * /_/ /_/____/ |__/|__/\____/|__/|__/   \__/_/ /_/\___/_/ /_/ /_/\___/
 *
 * ns.wow WordPress theme
 * by Neidhart + Schön Group
 */

// Bundle theme module (NSTheme)

import Helper from './Classes/Helper';
import FocusHandler from './Classes/FocusHandler';
import LazyLoad from 'vanilla-lazyload';
import ResponsiveTable from "./Classes/ResponsiveTable";
import XDownloader from "./Classes/XDownloader";

// import Accordion from './Classes/Accordion';
// import Jump from './Classes/Jump'
// import Sticky from '../../node_modules/sticky-js';
// import YoutubeLinks from './Classes/YoutubeLinks';

var focusHandlerInstance = null;
var lazyLoadInstance = null;
var responsiveTableInstance = null;
var downloaderInstance = null;

var accordionInstance = null;
var jumpInstance = null;
var youtubeLinksInstance = null;

export default {

  version: () => {
    return NSContext.theme.version;
  },

  lazyload: (givenNodeset) => {
    lazyLoadInstance.update(givenNodeset);
  }

  // addYoutubeLink: (videoContainer) => {
  //   youtubeLinksInstance.addLink(videoContainer);
  // }

};


// Initialize on document ready

document.addEventListener("DOMContentLoaded", function () {

  focusHandlerInstance = new FocusHandler();

  lazyLoadInstance = new LazyLoad();

  responsiveTableInstance = new ResponsiveTable({
    tableSelector: '.ns-table-wrap table',
    tableWrapperSelector: '.ns-table-wrap',
    breakPointLg: 1200,
    breakPointMd: 768,
    breakPointSm: 480,
    colCountNoSplit: 3,
    colCountEarlyBreak: 7,
    noSplitArray: []
  });

  downloaderInstance = new XDownloader({
    downloadText: "Download Excel", //Can be empty if you want to use an icon instead
    backgroundColor: "6ab04c", //Colored Cells Background
    backgroundClass: 'ns-backgroundcolor-gf_3a-green', //Class of colored cells
    borderColor: '95afc0', //Color of border
    borderStyle: 'thin', // thin/dotted/medium/double/thick
    headClass: 'head', //Class you want your columns based on. Will be bold in Excel
    boldClass: 'title', //Class for Bold text in Excel
    borderClass: 'ns-borderbottom-line_dots'
  });

  // accordionInstance = new Accordion({
  //   accordions: '.js-toggle-accordion'
  // });

  // jumpInstance = new Jump({
  //   graphicSelector: '.ns-graphic'
  // });

  // youtubeLinksInstance = new YoutubeLinks({
  // });

});
