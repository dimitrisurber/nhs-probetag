# nhs-probetag

Ich habe unser Standard Setup mit reingepackt damit du möglichst schnell starten kannst, der Gulp Default Task macht ziemlich alles was du dir wünschen kannst. 

Das Mockup am besten direkt ins web/app/index.html bauen.

Mach einfach so viel wie du innerhalb von einem Tag reinkriegst, Wichtigkeit in dieser Reihenfolge: 
- Grid (die Inhalte sind vernachlässigbar, wichtig ist das Layout & die groben Styles)
- Slider
- Header

Ich lasse das Layout bewusst unkommentiert um zu sehen wie du denkst, bei Fragen stehe ich aber natürlich zur Verfügung
