set :application, '@@VHOST@@'
set :repo_url, '@@GITREPO@@'
set :default_env, { path: "/usr/local/bin:$PATH" }

set :keep_releases, 3

set :cap_notify_emails, [ 'frontend-team@nsgroup.ch' ]
set :cap_notify_from, 'deploy@nsgroup.ch'
set :cap_notify_callsign, '@@VHOST@@'
set :cap_notify_latest_commit, proc { `git rev-parse HEAD`.strip }

# set :pty, true

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Sets branch to current one
#set :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master

set :deploy_to, -> { "/var/www/vhosts/#{fetch(:application)}" }

set :log_level, :info
# set :log_level, :debug

set :linked_files, %w{.env web/.htaccess wp-cli.yml}
set :linked_dirs, %w{web/app/uploads web/app/languages web/app/cache web/excels}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :service, :nginx, :reload
    end
  end

  desc "Send email notification"
  task :send_notification do
    Notifier.deploy_notification(self).deliver_now
  end

end

# after :deploy, 'deploy:send_notification'

# The above restart task is not run by default
# Uncomment the following line to run it on deploys if needed
# after 'deploy:publishing', 'deploy:restart'
# after 'deploy:publishing', 'memcached:restart'
