<?php

use Roots\WPConfig\Config;

Config::define( 'SAVEQUERIES', false );
Config::define( 'WP_DEBUG', false );
Config::define( 'WP_DEBUG_LOG', false );
Config::define( 'WP_DEBUG_DISPLAY', false );
Config::define( 'SCRIPT_DEBUG', false );

// Enforce SSL for login and admin.
Config::define( 'FORCE_SSL_ADMIN', true );
Config::define( 'FORCE_SSL_LOGIN', true );
