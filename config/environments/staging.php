<?php

use Roots\WPConfig\Config;

Config::define( 'SAVEQUERIES', true );
Config::define( 'WP_DEBUG', true );
Config::define( 'WP_DEBUG_LOG', true );
Config::define( 'WP_DEBUG_DISPLAY', false );
Config::define( 'SCRIPT_DEBUG', false );

// Enforce SSL for login and admin.
Config::define( 'FORCE_SSL_ADMIN', true );
Config::define( 'FORCE_SSL_LOGIN', true );

// Single sign-on debugging
// Config::define( 'AADSSO_DEBUG', true );
// Config::define( 'AADSSO_DEBUG_LEVEL', 100 );
