<?php

use Roots\WPConfig\Config;

Config::define( 'SAVEQUERIES', true );
Config::define( 'WP_DEBUG', true );
Config::define( 'WP_DEBUG_LOG', true );
Config::define( 'WP_DEBUG_DISPLAY', false );
Config::define( 'SCRIPT_DEBUG', true );

// Enforce SSL for login and admin.
Config::define( 'FORCE_SSL_ADMIN', false );
Config::define( 'FORCE_SSL_LOGIN', false );
