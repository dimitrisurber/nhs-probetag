export const config = {

  styles: {
    args: {
      src: './src/scss/*.scss',
      watch: './src/scss/**/*.scss',
      dest: ['./web/app/dist/js']
    }
  },

  scripts: {
    args: {
      src: './src/js/index.js',
      watch: './src/js/**/*.js',
      dest: './web/app/dist/js'
    }
  },


  scriptsPDF: {
    args: {
      src: './src/js/pdf.js',
      watch: './src/js/**/*.js',
      dest: './web/app/dist/js'
    }
  },

  fonts: {
    args: {
      src: './src/fonts/**/*.*',
      dest: [
        './web/app/dist/fonts/'
      ]
    }
  },

  img: {
    args: {
      src: './src/img/**/*.*',
      dest: [
        './web/app/dist/img/'
      ]
    }
  },

  favicons: {
    args: {
      src: './src/favicons/**/*.*',
      dest: [
        './web/app/dist/favicons/',
        './web/'
      ]
    }
  },

  svg: {
    args: {
      src: [
        'src/svg/**/*.svg'
      ],
      dest: ['./web/app/dist/svg']
    }
  },

  pot: {
    args: {
      src: [
        './web/app/src/**/*.php',
        './web/app/src/wp-cli/**/*.php',
        './web/app/src/wp-rest/**/*.php'
      ],
      dest: './web/app/languages/',
      domain: 'theme-text',
      package: `ns.wow Theme`
    }
  },

  lintjs: {
    args: {
      src: './src/js/**/*.js'
    }
  },

  lintcss: {
    args: {
      src: './src/scss/**/*.scss'
    }
  },

  lintphp: {
    args: {
      src: [
        './web/app/src/**/*.php',
        './web/app/src/wp-cli/**/*.php',
        './web/app/src/wp-rest/**/*.php'
      ]
    }
  },

  watch: {},
  watchPDF: {}

};
