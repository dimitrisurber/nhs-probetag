<?php
/**
 * Your base configuration goes in this file. Environment-specific overrides
 * go in their respective config/environments/{{WP_ENV}}.php file.
 *
 * A good default policy is to deviate from the production config as little as
 * possible. Try to define as much of your configuration in this file as you
 * can.
 */

use Dotenv\Dotenv;
use Roots\WPConfig\Config;

/* *** Directory layout *** */

$root_dir = dirname(__DIR__);

$webroot_dir = $root_dir . '/web';


/* *** Load environment *** */
/* *** Use Dotenv to parse the environment variables into the server context *** */

if ( ! file_exists( $root_dir . '/.env' ) ) {
	echo "<h1>Missing environment file</h1>";
	echo "<p>This project must have a .env file in the vhost's root directory.</p>";
	exit;
}

$dotenv = Dotenv::createImmutable($root_dir);
$dotenv->load();
$dotenv->required( [ 'DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL' ] );

// Current environment (defaults to "development")

define( 'WP_ENV', $_SERVER['WP_ENV'] ?: 'development' );

// All environment urls for site switching

define( 'ENVIRONMENTS', serialize( [
	'development' => $_SERVER['CFG_URL_DEVELOPMENT'],
	'staging' => $_SERVER['CFG_URL_STAGING'],
	'production' => $_SERVER['CFG_URL_PRODUCTION'],
] ) );


/* *** Reverse Proxy configuration *** */


// Prevent HTTPS redirection loop when site is behind a proxy with SSL termination

if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
	$_SERVER['HTTPS'] = 'on';
}


/* *** WordPress configuration *** */

Config::define( 'WP_MEMORY_LIMIT', '128M' );
Config::define( 'WP_MAX_MEMORY_LIMIT', ini_get( 'memory_limit' ) );

// URLs

Config::define('WP_HOME', $_SERVER['WP_HOME']);
Config::define('WP_SITEURL', $_SERVER['WP_SITEURL']);

// Custom Content Directory

Config::define('CONTENT_DIR', '/app');
Config::define('WP_CONTENT_DIR', $webroot_dir . Config::get('CONTENT_DIR'));
Config::define('WP_CONTENT_URL', Config::get('WP_HOME') . Config::get('CONTENT_DIR'));

// Database settings

Config::define('DB_NAME', $_SERVER['DB_NAME']);
Config::define('DB_USER', $_SERVER['DB_USER']);
Config::define('DB_PASSWORD', $_SERVER['DB_PASSWORD']);
Config::define('DB_HOST', $_SERVER['DB_HOST'] ?: 'localhost');
Config::define('DB_CHARSET', 'utf8mb4');
Config::define('DB_COLLATE', '');

$table_prefix = $_SERVER['DB_PREFIX'] ?: 'wp_';

// Authentication keys and salts

Config::define('AUTH_KEY', $_SERVER['AUTH_KEY']);
Config::define('SECURE_AUTH_KEY', $_SERVER['SECURE_AUTH_KEY']);
Config::define('LOGGED_IN_KEY', $_SERVER['LOGGED_IN_KEY']);
Config::define('NONCE_KEY', $_SERVER['NONCE_KEY']);
Config::define('AUTH_SALT', $_SERVER['AUTH_SALT']);
Config::define('SECURE_AUTH_SALT', $_SERVER['SECURE_AUTH_SALT']);
Config::define('LOGGED_IN_SALT', $_SERVER['LOGGED_IN_SALT']);
Config::define('NONCE_SALT', $_SERVER['NONCE_SALT']);

if ( isset( $_SERVER['JWT_AUTH_SECRET_KEY'] ) ) {
  Config::define( 'JWT_AUTH_SECRET_KEY', $_SERVER['JWT_AUTH_SECRET_KEY'] );
}

// S3 Media Offload (CDN)

if ( isset( $_SERVER['CDN_PROVIDER'] ) ) {
  Config::define( 'AS3CF_SETTINGS', serialize( array(
    'provider' => $_SERVER['CDN_PROVIDER'],
    'bucket' => $_SERVER['CDN_BUCKET'],
    'object-prefix' => $_SERVER['CDN_OBJECT_PREFIXBUCKET'],
    'access-key-id' => $_SERVER['CDN_ACCESS_KEY_ID'],
    'secret-access-key' => $_SERVER['CDN_SECRET_ACCESS_KEY'],
    'copy-to-s3' => '1',
    'enable-object-prefix' => '1',
    'force-https' => '1',
    'object-versioning' => '0',
    'region' => 'eu-central-1',
    'remove-local-file' => '1',
    'serve-from-s3' => '1',
    'use-yearmonth-folders' => '0'
  ) ) );
}

// Additional environment specific configuration

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if ( file_exists( $env_config ) ) {
	require_once $env_config;
}

Config::apply();

/* *** Bootstrap WordPress *** */

if (!defined('ABSPATH')) {
	define('ABSPATH', $webroot_dir . '/wp/');
}
